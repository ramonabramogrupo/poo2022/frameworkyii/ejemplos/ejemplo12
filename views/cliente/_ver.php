<div class="row">
    <div class="col-lg-12">
        <h2>Id: <?= $model->id ?><br></h2>
        <div class="text-white bg-primary rounded p-2">Nombre:</div>
        <div class="p-1"><?= $model->nombre ?></div>
        <div class="text-white bg-primary rounded p-2">Apellido1</div>
        <div class="p-1"><?= $model->apellido1 ?></div>
        <div class="text-white bg-primary rounded p-2">Apellido2</div>
        <div class="p-1 mb-5"><?= $model->apellido2 ?></div>
        <div class="p-1 mb-5">
            <?php
            // BOTON DE VER
            echo yii\helpers\Html::a(
                    '<i class="fal fa-eye"></i>', // icono
                    ['cliente/view', 'id' => $model->id], // controlador/accion y parametro
                    ['class' => 'btn btn-primary mr-2'] // estilos del boton
            );

            // BOTON DE ACTUALIZAR
            echo yii\helpers\Html::a(
                    '<i class="fas fa-pencil-ruler"></i>', // icono
                    ['cliente/update', 'id' => $model->id], // controlador/accion y parametro
                    ['class' => 'btn btn-primary mr-2'] // estilos
            );

            // BOTON DE ELIMINAR
            echo yii\helpers\Html::a(
                    '<i class="fad fa-trash"></i>', // icono
                    ['cliente/delete', 'id' => $model->id], // controlador/accion y parametro
                    [
                        'class' => 'btn btn-primary', // estilos
                        'data' => [
                            'confirm' => '¿Estas seguro que deseas eliminar el registro?',
                            'method' => 'post',
                        ], // esto solo para boton eliminar
                    ],
            );
            ?>
        </div>
        <br class="float-none">
    </div>
</div>

