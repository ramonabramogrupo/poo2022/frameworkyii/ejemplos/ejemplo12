<?php

use app\models\Cliente;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Administracion de Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-index">
    <p>
        <?= Html::a('Lista', ['indexg'], ['class' => 'btn btn-success']) ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => "_ver", // vista que se carga por cada registro
        "itemOptions" => [
                   'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
                ],
        "options" => [
                'class' => 'row',
                ],
        'layout' => "{items}{pager}",
        

    ]) ?>


</div>
