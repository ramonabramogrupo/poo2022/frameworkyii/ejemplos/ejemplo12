<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */

$this->title = "Mostrando el pedido con Id:" . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pedido-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estas seguro que deseas eliminar el pedido?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'total',
            'fecha',
            'id_cliente',
            //'cliente.nombre',
            //'cliente.apellido1',
            //'cliente.apellido2',
            'cliente.nombrecompleto',
            'id_comercial',
            //'comercial.nombre',
            //'comercial.apellido1',
            //'comercial.apellido2',
            'comercial.nombrecompleto'
        ],
    ]) ?>

</div>
