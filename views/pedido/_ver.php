<div class="row">
    <div class="col-lg-12">
        <h2>Id: <?= $model->id ?><br></h2>
        <div class="text-white bg-primary rounded p-2">Total:</div>
        <div class="p-1"><?= $model->total ?></div>
        <div class="text-white bg-primary rounded p-2">Fecha</div>
        <div class="p-1"><?= $model->fecha ?></div>
        <div class="text-white bg-primary rounded p-2">Comercial</div>
        <div class="p-1">Id: <?= $model->comercial->id ?></div>
        <div class="p-1">Nombre: <?= $model->comercial->nombre ?></div>
        <div class="p-1">Apellido1: <?= $model->comercial->apellido1 ?></div>
        <div class="p-1">Apellido2: <?= $model->comercial->apellido2 ?></div>
        <div class="text-white bg-primary rounded p-2">Cliente</div>
        <div class="p-1">Id: <?= $model->cliente->id ?></div>
        <div class="p-1">Nombre: <?= $model->cliente->nombre ?></div>
        <div class="p-1">Apellido1: <?= $model->cliente->apellido1 ?></div>
        <div class="p-1">Apellido1: <?= $model->cliente->apellido2 ?></div>
        <div class="p-1 mb-5">
            <?php
            // BOTON DE VER
            echo yii\helpers\Html::a(
                    '<i class="fal fa-eye"></i>', // icono
                    ['view', 'id' => $model->id], // controlador/accion y parametro
                    ['class' => 'btn btn-primary mr-2'] // estilos del boton
            );

            // BOTON DE ACTUALIZAR
            echo yii\helpers\Html::a(
                    '<i class="fas fa-pencil-ruler"></i>', // icono
                    ['update', 'id' => $model->id], // controlador/accion y parametro
                    ['class' => 'btn btn-primary mr-2'] // estilos
            );

            // BOTON DE ELIMINAR
            echo yii\helpers\Html::a(
                    '<i class="fad fa-trash"></i>', // icono
                    ['delete', 'id' => $model->id], // controlador/accion y parametro
                    [
                        'class' => 'btn btn-primary', // estilos
                        'data' => [
                            'confirm' => '¿Estas seguro que deseas eliminar el registro?',
                            'method' => 'post',
                        ], // esto solo para boton eliminar
                    ],
            );
            ?>
        </div>
        <br class="float-none">
    </div>
</div>

