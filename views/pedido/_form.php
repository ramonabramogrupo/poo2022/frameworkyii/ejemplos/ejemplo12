<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pedido-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form
            ->field($model, 'total')
            ->input(
                    "number",
                    [
                        "placeholder"=>"Introduce total del pedido"
                    ]) 
     ?>

    <?= $form
            ->field($model, 'fecha')
            ->input("date") 
    ?>

    <?= $form
            ->field($model, 'id_cliente') 
            ->dropDownList(
                    $model->listarclientes(),[
                     "prompt" => "Selecciona un cliente"   
                    ])
    ?>

    <?= $form
            ->field($model, 'id_comercial')
            ->listBox($model->listarcomerciales())
    ?>

    <div class="form-group">
        <?= Html::submitButton('Aceptar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
